
--Run challenge.m to evaluate the project.


--Run Start_GUI.m to explore more.    
  
In GUI you can:
  
1. See what happens during the free view point rendering.
     
    (Rectification, Disparity Map Estimation and View Generation)
  
2. Load Images(maximise 2 images) by mouse clicking.
  
3. Set the virtual view point by typing in the expected view point(0~1) or using the slider.
  
4. Save the generated image.