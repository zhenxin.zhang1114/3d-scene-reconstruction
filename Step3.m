Irect1 = evalin('base','Irect1');   % Load stereo pair from the base workspace
Irect2 = evalin('base','Irect2');
stereo_graph = (stereoAnaglyph(Irect1,Irect2));
S.fh = figure('units','pixels',...   % cearte the GUI
              'position',[200 200 900 700],...
              'menubar','none',...
              'numbertitle','off',...
              'name','Red-cyan composite view of the stereo images',...
              'resize','off');
S.ax1 = axes('units','pixels',...
            'position',[50 130 800 500]);
S.R1 = imshow(stereo_graph);  % Display the image on S.ax.

S.pb_2 = uicontrol('style','push',...    % Push to get disparity maps
                 'units','pix',...
                 'position',[300 70 300 40],...
                 'backgroundcolor','w',...
                 'HorizontalAlign','left',...
                 'string','Get Disparity Maps',...
                 'fontsize',14,'fontweight','bold',...
                 'callback',{@pb_call_2,S});
             
function [] = pb_call_2(varargin)
addpath('Disparity And New View');
  Irect1 = evalin('base','Irect1');
  Irect2 = evalin('base','Irect2');
  set = evalin('base','set');
 [dispL,dispR] = EstimateDispatity4gui(Irect1,Irect2,set);
  
 assignin('base','dispL',dispL);
 assignin('base','dispR',dispR);
 closereq;
 Step4();
 end