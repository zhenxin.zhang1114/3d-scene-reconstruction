%% Computer Vision Challenge

% Groupnumber:
group_number = 'G38';

% Groupmembers:
% members = {'Max Mustermann', 'Johannes Daten'};
members = {'Ji Chen','Hongxu Liu','Yuanzheng Fang',...
           'Zhenxin Zhang', 'Huiwen Pan'};

% Email-Adress (from Moodle!):
% mail = {'ga99abc@tum.de', 'daten.hannes@tum.de'};
mail = {'ji.chen@tum.de','ga27jon@mytum.de','ge69toc@mytum.de',...
        'ge69pav@mytum.de','ga27keq@tum.de'};

%% Load images
clc;
clear all;
close all;
addpath('Images');
image1 = 'L2.JPG';
image2 = 'R2.JPG';
%% Free Viewpoint Rendering
% start execution timer -> tic;
tic;
[output_image]  = free_viewpoint(image1, image2, 0.7); %
toc;
% stop execution timer -> toc;
elapsed_time = '114.094887 seconds on MacBook Pro 13, i5';

%% Display Output
% Display Virtual View
figure;
imshow(output_image);
title('Virtual View');
