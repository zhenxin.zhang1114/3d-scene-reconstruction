%% Notations
% This function gives the whole process of stereo rectification
% -Inputs:
%    LeftImageName, RightImageName: full name of images('L1.JPG').
% -Outputs:
%    Irect1,Irect2: rectified stereo pair.
%    set: determin which stereo pair was processed
%%
function  [Irect1,Irect2,set]= rectification(LeftImageName, RightImageName)
        addpath('Stereo Rectification/Rect');
    if contains(LeftImageName,'L1')&&contains(RightImageName,'R1')
        disp('got L1,R1');
 %% Read Images and Load Parameters      
        image1 = imread(LeftImageName);
        image2 = imread(RightImageName);
        T1 = load('T1');
        R1 = load('R1');
        K1 = load('K1');      
        T_com = -T1.T;
        R_com = R1.R;
        K_com = K1.K;
        mxl = -660;
        mxr = 750;
        my = -100;
        h_ = 1850;
        w_ = 3000;
 %% Compute the transformation matrix and do the rectification
        [Rl,Rr] = rectify_transformation(T_com,R_com);
        disp('Computing rectified Images');
        Irect1 = pre_warp_backward(image1,Rl*R_com,K_com,mxl, my, h_, w_);
        Irect2 = pre_warp_backward(image2,Rl,K_com,mxr,my, h_, w_);
        set = 1;
        figure;
        imshow(stereoAnaglyph(Irect1,Irect2));
        title('Red-cyan composite view of the stereo images');
        
    elseif contains(LeftImageName,'L2')&&contains(RightImageName,'R2')
        disp('got L2 R2');
 %% Read Images and Load Parameters 
        image1 = imread(LeftImageName);
        image2 = imread(RightImageName);
        T2 = load('T2');
        R2 = load('R2');
        K2 = load('K2');      
        T_com = -T2.T;
        R_com = R2.R;
        K_com = K2.K;
        mxl = -667;%
        mxr = 300 ; 
        my = -40; 
        h_ = 1950;
        w_ = 2850;
  %% Compute the transformation matrix and do the rectification
        [Rl,Rr] = rectify_transformation(T_com,R_com);
        disp('Computing rectified Images');
        Irect1 = pre_warp_backward(image1,Rl*R_com,K_com,mxl, my, h_, w_);
        Irect2 = pre_warp_backward(image2,Rl,K_com,mxr,my, h_, w_);
        set = 2;
        figure;
        imshow(stereoAnaglyph(Irect1,Irect2));
        title('Red-cyan composite view of the stereo images');
    else 
        disp('Please Load the stereo pair from left to right!');
    end
end