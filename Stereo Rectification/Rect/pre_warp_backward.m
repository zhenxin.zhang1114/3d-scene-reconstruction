%% Notations
% This function warps the image to a desired plane using planar
% transformation (backwards)
% -Inputs:I:Original image; 
%         H:Rotation matrix; 
%         K:Matrix about camera information
%         mx, my: Movement of the warped image in x and y axis to obtain
%                 just the overlap parts of both images
%         h_,w_: Height and weight of the warped image after cutting off 
%                the useless parts
% -Outputs: I_prwd: Warped image.
%%
function I_prwd = pre_warp_backward(I, H, K, mx, my, h_, w_) 
%I:[500 500], H:[3 3]
[h, w, d] = size(I);
%I_prwd = zeros(h, w);
I_prwd = zeros(h_,w_, 3);
H_inv = inv(H);
pv = ones(3, 1);
K_inv = inv(K);
%color = 0.299*198 + 0.587*208 + 0.114*224;
for x = 1:w_
    for y = 1:h_
        pm_pixel = [x-mx; y-my; 1.0];
        pm = K_inv*pm_pixel; %% pixel coordinates to image coordinates
        pv = H_inv * pm;     %% backward calculation
        pv = pv/pv(3);
        pv_pixel = K*pv;    %% image coordinates to pixel coordinates
        xs = round(pv_pixel(1)); 
        ys = round(pv_pixel(2));

        if(xs>=1 && xs<=w && ys>=1 && ys<=h)
            I_prwd(y,x,:) = I(ys,xs,:);
        end

        
    end

end
I_prwd = uint8(I_prwd);
end

