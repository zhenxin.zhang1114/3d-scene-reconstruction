%% Notations
% this function computes the rectify transformation using Bouguet method.
% -Inputs: 
%      T,R: extrinstic parameters of the camera system.
% -Outputs:
%      Rl: rectify transformation
%% 
function [Rl,Rr]=rectify_transformation(T,R)
    e1 = T/norm(T);
    e2 = [-T(2),T(1),0]'/sqrt(T(1)^2+T(2)^2);
    e3 = cross(e1,e2);
    Rl = [e1';e2';e3'];
    Rr = R*Rl;
end