function [] = GUI_13()
% Demonstrate how to display & change a slider's position with an edit box.  
% Slide the slider and it's position will be shown in the editbox.  
% Enter a valid number in the editbox and the slider will be moved to that 
% position.  If the number entered is outside the range of the slider, 
% the number will be reset.
%
%
% Author:  Matt Fig, Edit by Ji Chen
% Date:  7/15/2009

S.fh = figure('units','pixels',...
              'position',[300 300 600 500],...
              'menubar','none',...
              'name','GUI_13',...
              'numbertitle','off',...
              'resize','off');
t = annotation('textbox','String','Please move the slider and wait for the result',...
                    'Position',[0.03 0.1 0.1 0.1]);
t.FontSize = 13;
S.sl = uicontrol('style','slide',...
                 'unit','pix',...
                 'position',[20 3 260 30],...
                 'min',0,'max',1,'val',0.1);             
S.ed = uicontrol('style','edit',...
                 'unit','pix',...
                 'position',[20 40 260 30],...
                 'fontsize',16,...
                 'string','0.1');
set([S.ed,S.sl],'call',{@ed_call,S});  % Shared Callback.
S.pb_2 = uicontrol('style','push',...
                 'units','pix',...
                 'position',[350 40 200 40],...
                 'backgroundcolor','w',...
                 'HorizontalAlign','left',...
                 'string','Save The Image',...
                 'fontsize',14,'fontweight','bold',...
                 'callback',{@pb_call_2,S});
end

function [] = ed_call(varargin)
% Callback for the edit box and slider.
[h,S] = varargin{[1,3]};  % Get calling handle and structure.
 dispL =evalin('base','dispL');
 dispR = evalin('base','dispR');
 set_number = evalin('base','set');
 left_image = evalin('base','Irect1');
 right_image = evalin('base','Irect2');
switch h  % Who called?
    case S.ed
        L = get(S.sl,{'min','max','value'});  % Get the slider's info.
        E = str2double(get(h,'string'));  % Numerical edit string.
        if E >= L{1} && E <= L{2}
            set(S.sl,'value',E)  % E falls within range of slider.
            pause(5);
            S.ax1 = axes('units','pixels',...
                            'position',[35 100 520 350]);
            final_view = get_newview4gui(dispL,dispR,...
                        left_image,right_image,set_number,E);
            S.R1 = imshow(final_view);  % Display the image on S.ax.

        else
            set(h,'string',L{3}) % User tried to set slider out of range.
        end
    case S.sl
        set(S.ed,'string',get(h,'value')) % Set edit to current slider.
        S.ax1 = axes('units','pixels',...
            'position',[35 100 520 350]);
        final_view = get_newview4gui(dispL,dispR,...
                        left_image,right_image,set_number,get(h,'value'));
        assignin('base','virtual_view',final_view);
        S.R1 = imshow(final_view);  % Display the image on S.ax.
    otherwise
        % Do nothing, or whatever.
end
end

function [] = pb_call_2(varargin)
 views = evalin('base','virtual_view');
 views = imresize(views,[2000 3000]);
 imwrite(views,'virtual_view.JPG');
 
 
 end