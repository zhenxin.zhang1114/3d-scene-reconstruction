try
   L1 =evalin('base','L1');           %Load images from base workspace
   R1 = evalin('base','R1');
   set = 1;
   disp('got L1 R1')
   assignin('base','L',L1);
   assignin('base','R',R1);
   assignin('base','set',set);
catch exception
   disp('did not got L1 and R1');
end

try
  L2 = evalin('base','L2');
  R2 = evalin('base','R2');
  set = 2;
  disp('got L2 R2');
  assignin('base','L',L2);
  assignin('base','R',R2);
  assignin('base','set',set);
catch
  disp('did not got L2 and R2');
end

S.fh = figure('units','pixels',...            % Show the selected images
              'position',[200 200 1000 400],...
              'menubar','none',...
              'numbertitle','off',...
              'name','Selected Images',...
              'resize','off');
t = annotation('textbox','String','Please press the button and wait for the result.(takes about 30 seconds)',...
                    'Position',[0.56 0.01 0.1 0.1]);
t.FontSize = 13;
S.ax1 = axes('units','pixels',...
            'position',[10 80 450 300]);
S.R1 = imshow(evalin('base','L'));  % Display the image on S.ax.
S.ax2 = axes('units','pixels',...
            'position',[510 80 450 300]);
S.R2 = imshow(evalin('base','R'));

S.pb_2 = uicontrol('style','push',...         % Push to start the rectification
                 'units','pix',...
                 'position',[250 10 300 40],...
                 'backgroundcolor','w',...
                 'HorizontalAlign','left',...
                 'string','Start Rectification',...
                 'fontsize',14,'fontweight','bold',...
                 'callback',{@pb_call_2,S});
             
 function [] = pb_call_2(varargin)
 addpath('Stereo Rectification');
  L = evalin('base','L');
  R = evalin('base','R');
  set = evalin('base','set');
 [Irect1,Irect2]= rectification4gui(L, R,set);
  
 assignin('base','Irect1',Irect1);
 assignin('base','Irect2',Irect2);
 closereq;
 Step3();
 end
