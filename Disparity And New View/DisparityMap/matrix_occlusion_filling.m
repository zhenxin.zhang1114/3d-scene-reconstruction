%% Notations
%  this function fills the detected occlusion . 
%  It reduces the horizontal voice by area 
%  searching concluded in horizontal searching.  
%  -Inputs: 
%     dispMap: disparity map with labeled holes
%     value_of_hole: the value of holes
%     vertical_scale: specify the heght of block area in searching
%  -Outputs:
%     new_dispMap: the filled disparity map 
%%
function new_dispMap = matrix_occlusion_filling(dispMap,value_of_hole,verticalscale)
new_dispMap = dispMap;
    [r,c] = size(dispMap);
    disp('starting the occlusion filling...')
    for i=verticalscale+1:r-verticalscale
        for j=1:c
            if dispMap(i,j) == value_of_hole  % detected a hole
                row = new_dispMap(i,:);
                index = find(row~=value_of_hole);  % got all index of all pixels along the row which are not holes.
                index_left = index(index<j);     
                
               
                    
                index_right = index(index>j);
                if numel(index_right) > 0 && numel(index_left)>0

                    left_disp = 0;
                    if numel(index_left)<5
                        left_mat = [];
                        for k = numel(index_left)-numel(index_left)+1:numel(index_left)
                            left_mat = [left_mat,new_dispMap(...
                            i-verticalscale:i+verticalscale,index_left(k))];   %specify the searching area on the left of the hole                      
                        end
                        mat_ind = find(left_mat>value_of_hole);
                        leftdisp_sum = 0;
                        for ind_index = 1:numel(mat_ind)
                            leftdisp_sum = leftdisp_sum + left_mat(mat_ind(ind_index));
                        end
                        
                        left_disp = leftdisp_sum/numel(mat_ind); %left side mean disparity
                    
                    elseif numel(index_left)>5
                        left_mat = [];
                        for k = numel(index_left)-5+1:numel(index_left)
                            left_mat = [left_mat,new_dispMap(...
                            i-verticalscale:i+verticalscale,index_left(k))];  %specify the searching area on the left of the hole                       
                        end                        
                        for k = numel(index_left)-5+1:numel(index_left)
                            left_disp = left_disp + row(index_left(k));
                        end
                        mat_ind = find(left_mat>value_of_hole);
                        leftdisp_sum = 0;
                        for ind_index = 1:numel(mat_ind)
                            leftdisp_sum = leftdisp_sum + left_mat(mat_ind(ind_index));
                        end
                        
                        left_disp = leftdisp_sum/numel(mat_ind); %left side mean disparity
                    end
                                       
                    
                    right_disp = 0;
                    if numel(index_right)<5
                        right_mat = [];
                        for k = 1:numel(index_right)
                            right_mat = [right_mat,new_dispMap(...
                            i-verticalscale:i+verticalscale,index_right(k))];   %specify the searching area on the right of the hole                      
                        end
                        mat_ind = find(right_mat>value_of_hole);
                        rightdisp_sum = 0;
                        for ind_index = 1:numel(mat_ind)
                            rightdisp_sum = rightdisp_sum + right_mat(mat_ind(ind_index));
                        end
                        
                        right_disp = rightdisp_sum/numel(mat_ind); % right-side disparity

                    elseif numel(index_right)>5
                        right_mat = [];
                        for k = 1:5
                            right_mat = [right_mat,new_dispMap(...
                            i-verticalscale:i+verticalscale,index_right(k))];    %specify the searching area on the left of the hole                     
                        end
                        mat_ind = find(right_mat>value_of_hole);
                        rightdisp_sum = 0;
                        for ind_index = 1:numel(mat_ind)
                            rightdisp_sum = rightdisp_sum + right_mat(mat_ind(ind_index));
                        end
                        
                        right_disp = rightdisp_sum/numel(mat_ind);% right-side disparity
                    end
                                                  
                    new_disparity = min(left_disp,right_disp); % the disparity of the occlusion can be defined as the smaller disparity
                    new_dispMap(i,j) = new_disparity;
                end
                
            end
        end
    end
    disp('occlusion filling finished!')
end
    
    

