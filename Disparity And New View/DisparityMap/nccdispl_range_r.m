%% Notations
%this function returns the disparity map of right image
%inputs: 
%        d:disparityrange
%        winsize : size of matching blocks in left and right image(odd number)
%        threashhold: describes the reasonable disparity
%output:
%        dispartymap: disparitymap of left image
%--------------------------------------------------------------------------
%% 
function disparitymap = nccdispl_range_r(leftimg,rightimg,d, winsize,threashhold)
[nrright,ncright] =size(rightimg);
d_min = d(1);
d_max = d(2);
%winsize = 11;
disparitymap = zeros(nrright,ncright);
leftimg = double(leftimg);
rightimg = double(rightimg);
leftimg = supplyborder(leftimg,winsize);
rightimg = supplyborder(rightimg,winsize);
win = (winsize-1)/2;
hwait=waitbar(0,'calculating the disparity of right image');
for i = 1+win:nrright+win
    waitbar(i/(nrright+win),hwait,'calculating the disparity of right image');
    for j = 1+win:ncright+win
        min = -9999999;
        recordk = 0;
        for k = d_min:d_max
            numerator = 0.0;
            powerrightwin = 0.0;
            powerleftwin = 0.0;
            for a=-win:win
                for b = -win:win
                    if j+b+k < ncright && j+b+k>0
                        numerator = numerator+(leftimg(i+a,j+b+k)*rightimg(i+a,j+b));
                        powerleftwin = powerleftwin+(leftimg(i+a,j+b+k)*leftimg(i+a,j+b+k));
                        powerrightwin = powerrightwin+(rightimg(i+a,j+b)*rightimg(i+a,j+b));
                    end
                end
            end
            ncctemp = numerator/(sqrt(powerrightwin*powerleftwin));
            if (min < ncctemp )
                min = ncctemp;
                if abs(k)<threashhold
                    recordk = k;
                end
                
            end
        end
        disparitymap(i-win,j-win) = recordk;
    end
end
close(hwait);
end

%disparitymap = uint8(3*disparitymap);
%output_img = disparitymap;
% imshow(disparitymap);
% imwrite(disparitymap,strcat('C:\Users\samsung-\Desktop\output\nccdisp1.png'));
