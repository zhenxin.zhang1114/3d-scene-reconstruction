%% Notations  
% this function provides the whole process of disparity map
% Estimation.
% -Inputs:
%  imageL,imageR:rectified stereo pair
%  dispRange: disparity range, a (1,2) vector contains maximal
%             and minimal disparity
%  distThreash: distance threashhold, a reliable value of
%             of disparity. (max_disp>=distThreash>=min_disp) 
%  Compressed_size: new size of input images.
% -Outputs:
%  LMMOF,RMMOF: filled and filtered disparity maps.
%%
function [LMMOF,RMMOF] = get_disparityMap(imageL,imageR,...
                               dispRange,winSize,distThreash,Compressed_size)
%% resize 3/10
  I1_resized = imresize(imageL, Compressed_size);
  I2_resized = imresize(imageR, Compressed_size);
  %imshow(stereoAnaglyph(I1_resized,I2_resized));
  image1 = rgb_to_gray(I1_resized);
  image2 = rgb_to_gray(I2_resized);
%% Calculate the Disparity Map
  dispL = nccdispl_range(image1,image2,dispRange, winSize,distThreash);
  dispR = nccdispl_range_r(image1,image2,dispRange, winSize,distThreash);
%% Hole filling
  [dL,dR] = LRC_check(dispL,dispR,20,-500);  %set the occlision(holes) to the value -500
  LMOF = matrix_occlusion_filling(dL,-500,3); % matrix occlusion filling
  RMOF = matrix_occlusion_filling(dR,-500,3);
  disp('executing the medfilt...');
  LMMOF = medfilt2(LMOF,[3 3]);              % medfiltering reduce the noise
  RMMOF = medfilt2(RMOF,[3 3]);
  disp('medfiltering finished!');

end