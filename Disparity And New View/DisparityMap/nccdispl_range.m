%% Notations 
%this function returns the disparity map of left image
%inputs: 
%        d:disparityrange
%        winsize : size of matching blocks in left and right image(odd number)
%        threashhold: describes the reasonable disparity
%output:
%        dispartymap: disparitymap of left image
%--------------------------------------------------------------------------
%%
function disparitymap = nccdispl_range(leftimg,rightimg,d, winsize,threashhold)
[nrright,ncright] =size(rightimg);
d_min = d(1);   %minimum of disparity range (negative value)
d_max = d(2);   %maximum of disparity range (positive value)
%winsize = 11;
disparitymap = zeros(nrright,ncright);
leftimg = double(leftimg);
rightimg = double(rightimg);
leftimg = supplyborder(leftimg,winsize);     %%generate borders in left image
rightimg = supplyborder(rightimg,winsize);   %%in order to prevent the exceeding of array bounds 
win = (winsize-1)/2;        %%winsize should be odd number
hwait=waitbar(0,'calculating the disparity of left image'); %initialize the progress bar
for i = 1+win:nrright+win
    waitbar(i/(nrright+win),hwait,'calculating the disparity of left image');
    for j = 1+win:ncright+win
        min = -9999999;
        recordk = 0;
        for k = d_min:d_max
            numerator = 0.0;
            powerrightwin = 0.0;
            powerleftwin = 0.0;
            for a=-win:win
                for b = -win:win
                    if j+b-k > 0 && j+b-k<ncright
                        numerator = numerator+(leftimg(i+a,j+b)*rightimg(i+a,j+b-k));
                        powerleftwin = powerleftwin+(leftimg(i+a,j+b)*leftimg(i+a,j+b));
                        powerrightwin = powerrightwin+(rightimg(i+a,j+b-k)*rightimg(i+a,j+b-k));
                    end
                end
            end
            ncctemp = numerator/(sqrt(powerrightwin*powerleftwin));%% calculate NCC between left and right block
            if (min < ncctemp )   
                min = ncctemp;     %save the bigger NCC
                if abs(k)<threashhold   % if the disparity is resonable
                    recordk = k;        % record the disparity
                end
                
            end
        end
        disparitymap(i-win,j-win) = recordk;
    end
end
close(hwait);     %shutdown the progress bar
end

%disparitymap = uint8(3*disparitymap);
%output_img = disparitymap;
% imshow(disparitymap);
% imwrite(disparitymap,strcat('C:\Users\samsung-\Desktop\output\nccdisp1.png'));
