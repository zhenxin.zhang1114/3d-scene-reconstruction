%% Notations
% This function finds the occlusion area in Left- and right-
% disparity-map.
% Eg. Using the information of Left-disparity-map, we can generate a
% new right-disparity-map, then we compare the difference between
% the new right-disparity-map and right-disparity-map, if the 
% difference aboves the threashhold we consider the occlusion
% occurs. 
% -Inputs: dispL and dispR: disparity maps
%          threshhold: the threashhold of occlision, when disparity-
%                difference exceeds the threashhold, the pixel is
%                considered as a hole.
%          value_of_hole: the specific value of holes, usually
%                smaller than the minimal disparity or larger than
%                the maximal disparity.
% -Outputs: dL and dR: processed disparity maps with holes
%%
function [dL,dR] = LRC_check(dispL,dispR,threshhold,value_of_hole)
disp('LR-Checking...');
dL = dispL;
[r,c] = size(dL);
for i = 1:r
    for j = 1:c
        d1 = dispL(i,j);
        new_j = j-d1;
        new_j = floor(new_j);
        if new_j>0 && new_j<c
            d2 = dispR(i,new_j);
            if abs(d1-d2)>threshhold 
                dL(i,j) = value_of_hole;
            end
        end
    end
end

dR = dispR;
[r,c] = size(dR);
for i = 1:r
    for j = 1:c
        d1 = dispR(i,j);
        new_j = j+d1;
        new_j = floor(new_j);
        if new_j>0 && new_j<c
            d2 = dispL(i,new_j);
            if abs(d1-d2)>threshhold
                dR(i,j) = value_of_hole;
            end
        end
    end
end
disp('LR-Checking finished!');
end
