%% Notations
%  This function supplies the border around the image. 
%  -Inputs: 
%      img: imput image
%      windsize: the width of border
%% 
function img_new = supplyborder(img,winsize)
    [rows, columns] = size(img);
    img_new = zeros(rows+2*winsize, columns+2*winsize);
    [rows_new,columns_new] = size(img_new);
    img_new(winsize+1:rows_new-winsize,winsize+1:columns_new-winsize ) = img;

end