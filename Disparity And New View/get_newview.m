%% Notations
%This Function Provides tunnels to estimate the get virtual view
%of different stereo pairs (disparity maps).
%In order to get the result quickly, the image must be resized 
%smaller.
%After the stereo rectification the stereo pair must be cropped.
%Relative sizes of stereo pair 1 and stereo pair 2 are different
%In order to keep the scale of both stereo pairs, the resize 
%parameters are different. 
%-Inputs: Image1 and Image2: rectified stereo pair.
%         dispL and dispR: disparity maps for left and right images
%         set: determins which image pair is used(L1,R1 or L2,R2).
%-OutPuts: final_view: virtual view.
%%
function [final_view] = get_newview(dispL,dispR,Image1,Image2,set,theta)
    addpath('Disparity And New View/View_Generation');    
    if set==1
        compressed_size = [555 900];
        imageL = imresize(Image1,compressed_size);
        imageR = imresize(Image2,compressed_size);
        [final_view] = new_view(dispL,dispR,imageL,imageR,theta);
    else
        compressed_size = [585 855];%585 855
        imageL = imresize(Image1,compressed_size);
        imageR = imresize(Image2,compressed_size);
        [final_view] = new_view(dispL,dispR,imageL,imageR,theta);
    end
    
end