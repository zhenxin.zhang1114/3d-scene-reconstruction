%% Notations
%This Function Provides tunnels to estimate the disparity of 
%different stereo pairs.
%In order to get the result quickly, the image must be resized 
%smaller.
%After the stereo rectification the stereo pair must be cropped.
%Relative sizes of stereo pair 1 and stereo pair 2 are different
%In order to keep the scale of both stereo pairs, the resize 
%parameters are different. 
%-Inputs: I_rect1 and I_rect2: rectified stereo pair.
%         set: determins which image pair is used(L1,R1 or L2,R2).
%-OutPuts: dispL and dispR: disparity maps.
%% 
function [dispL,dispR]=EstimateDispatity(I_rect1,I_rect2,set)
 addpath('Disparity And New View/DisparityMap');
 
 imageL = I_rect1;
 imageR = I_rect2;
 if set == 1
 Compressed_size = [555 900];
 I1_resized = imresize(imageL, Compressed_size); 
 I2_resized = imresize(imageR, Compressed_size); 
  %% calculate disparity maps
 [dispL,dispR] = get_disparityMap(I1_resized,I2_resized,[-150 150],...
                                   5,250,Compressed_size);
 %% show 
  figure;
  subplot(1,2,1);
  imshow(dispL,[-150 150]);
  title('Disparity Map of Left Image')
  subplot(1,2,2);
  imshow(dispR,[-150 150]);
  title('Disparity Map of Right Image')
 else
 Compressed_size = [585 855];
 I1_resized = imresize(imageL, Compressed_size); 
 I2_resized = imresize(imageR, Compressed_size); 
 %% calculate disparity maps
 [dispL,dispR] = get_disparityMap(I1_resized,I2_resized,[-150 150],...
                                   5,350,Compressed_size);
  %% show 
  figure;
  subplot(1,2,1);
  imshow(dispL,[-150 150]);
  title('Disparity Map of Left Image')
  subplot(1,2,2);
  imshow(dispR,[-150 150]);
  title('Disparity Map of Right Image')
 end

end

 

