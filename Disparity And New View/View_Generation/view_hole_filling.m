%% Notations
% This function fills the holes caused by noise, the value of 
%these holes must be given.
% -Inputs: winsize: window size around the hole, must be odd.
%          image: input image with sigle channel.
%          value_of_hole: usually: 0.
% -Output:
%          new_view: the filled image

%% 
function new_view=view_hole_filling(image,window_size,value_of_hole)
    new_view = image;
    [r,c] = size(image);
    half_winsize = floor(window_size/2);
    
    for i=1+half_winsize:r-half_winsize
        for j=1+half_winsize:c-half_winsize
            if image(i,j) == value_of_hole % detected the hole
               sum_block = 0;
               window = image(i-half_winsize:i+half_winsize,...
               j-half_winsize:j+half_winsize); % window around the hole
               index = find(window~=value_of_hole); % the element in this window which their values unequal to the value of hole.
               
               for k=1:numel(index)
                   sum_block = sum_block+window(index(k));
               end
               mean_block = sum_block/numel(index);  % new value of this pixel.
               new_view(i,j) = mean_block;
            
            end
        end
    end
   
end


