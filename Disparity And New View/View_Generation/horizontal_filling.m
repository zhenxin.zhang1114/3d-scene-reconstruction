%% Notations
% This function is used for hole filling and image smoothing
% without knowing the value of holes
% -Inputs: Oldimage: input sigle-channel image
%          searching_scale: searching area of the leftside and rightside
%               of a pixel    
%          threashhold: specify if 2 area are on the same plane. 
% -Output: Newimage: Smoothed and filled image.
%% 
function Newimage = horizontal_filling(Oldimage,searching_scale,threashhold)
    [r,c] = size(Oldimage);
    Newimage = double(Oldimage);
    for i=1:r
        for j=searching_scale+1:c-searching_scale
            leftvector = Newimage(i,j-searching_scale:j-1); %left search area
            rightvector = Newimage(i,j+1:j+searching_scale);%right sreach area
            left_color = sum(leftvector)/searching_scale; 
            template = left_color*ones(1,searching_scale);% use mean value of left area as template
            difference = abs(template-rightvector);
            if min(difference)<threashhold   %% left and right area blongs to the same plane
                Newimage(i,j) = left_color;  
            else
                continue;
            end
          
        end
    end
end