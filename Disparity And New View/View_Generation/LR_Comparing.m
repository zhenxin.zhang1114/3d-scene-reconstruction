%% Notations
% This function compares virtual views generated from left- and right-
% disparity maps and generates the combined new view.
% -Inputs:
%  viewFromL: virtual view generated from left-disparity-map.
%  viewFromR: virtual view generated from right-disparity-map.
%  theta: the view point of virtual view (0,1)
% -Outputs:
%  final_view: combined new virtual view
%% 
function final_view = LR_Comparing(viewFromL,viewFromR,theta)
    [r,c] = size(viewFromL);
    
 if theta < 0.5   %if view point close to left camera, use left camera as reference.
    final_view = viewFromL;
    for i=1:r
        for j=1:c
            if viewFromL(i,j) == 0 && viewFromR(i,j)~=0
                final_view(i,j) = viewFromR(i,j);  % filling the holes
            end
            
        end
    end
 else 
     final_view = viewFromR; %otherwise use right camera as reference.
     for i=1:r
        for j=1:c
            if viewFromR(i,j) == 0 && viewFromL(i,j)~=0
                final_view(i,j) = viewFromL(i,j);
            end
            
        end
     end
    
end