%% Notations
% This function provied the whole process of generating the new view
% -Inputs: 
%    dispL, dispR: disparity maps of left and right images.
%    imageL,imageR: rectified stereo pair.
%    theta: position of intermediate camera.
% -Ourput:
%    final_view: the image "taken" from the virtual view point.

%%
function [final_view]=new_view(dispL,dispR,imageL,imageR,theta)
disp('Generating new views...');
    [r,c,cn] = size(imageL);
    newL = zeros(r,c,cn);
    for i=1:r
        for j=1:c
            shifting = dispL(i,j);
            new_j = floor(j-shifting*theta);
                if new_j > 0 && new_j < c
                newL(i,new_j,:) = imageL(i,j,:);
                end
        end
    end

    newR = zeros(r,c,cn);
    for i=1:r
        for j=1:c
            ci = r-i+1;
            cj = c-j+1;
            shifting = dispR(ci,cj);
            new_j = floor(cj+shifting*(1-theta));
            if new_j < c && new_j > 0
                newR(ci,new_j,:) = imageR(ci,cj,:);
            end
        end
    end
    
%% Comparing same views generated from Left and Right Image
disp('LR-Comparing...')
finalview_rough = LR_Comparing(newL,newR,theta);
%% Hole Filling (Know Value of Holes(0))
disp('Single-View Hole Filling...')
final_view_r = view_hole_filling(finalview_rough(:,:,1),4,0);
final_view_g = view_hole_filling(finalview_rough(:,:,2),4,0);
final_view_b = view_hole_filling(finalview_rough(:,:,3),4,0);
final_view_r = medfilt2(final_view_r,[4 4]);
final_view_g = medfilt2(final_view_g,[4 4]);
final_view_b = medfilt2(final_view_b,[4 4]);

%% Hole Filling (Don't know value of holes)
new_rv = vertical_filling(final_view_r,3,5);
new_gv = vertical_filling(final_view_g,3,5);
new_bv = vertical_filling(final_view_b,3,5);

newr = horizontal_filling(new_rv,3,5);
newg = horizontal_filling(new_gv,3,5);
newb = horizontal_filling(new_bv,3,5);

final_view = cat(3, newr, newg, newb);
disp('finished!');
final_view = uint8(final_view);
end

