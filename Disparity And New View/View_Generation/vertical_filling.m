%% Notations
% This function is used for hole filling and image smoothing
% without knowing the value of holes
% This function applies the same method used im horozontal_filling.m
% The searching direction is vertical
% -Inputs: Oldimage: input sigle-channel image
%          searching_scale: searching area of the upper- and under-side
%               of a pixel    
%          threashhold: specify if 2 area are on the same plane. 
% -Output: Newimage: Smoothed and filled image.

%% 
function Newimage = vertical_filling(Oldimage,searching_scale,threashhold)
    [r,c] = size(Oldimage);
    Newimage = double(Oldimage);
    for j=1:c
        for i=searching_scale+1:r-searching_scale
            uppervector = Newimage(i-searching_scale:i-1,j);
            undervector = Newimage(i+1:i+searching_scale,j);
            upper_color = sum(uppervector)/searching_scale;
            template = upper_color*ones(searching_scale,1);
            difference = abs(template-undervector);
            if min(difference)<threashhold
                Newimage(i,j) = upper_color;
            else
                continue;
            end
          
        end
    end
end