%% Notations
%The only difference between EstimateDisparity and this
%function is that this function contains no image-plotting. 
%%
function [dispL,dispR]=EstimateDispatity4gui(I_rect1,I_rect2,set)
 addpath('Disparity And New View/DisparityMap');
 
 imageL = I_rect1;
 imageR = I_rect2;
 if set == 1
 Compressed_size = [555 900];
 I1_resized = imresize(imageL, Compressed_size); 
 I2_resized = imresize(imageR, Compressed_size); 
 [dispL,dispR] = get_disparityMap(I1_resized,I2_resized,[-150 150],...
                                   5,250,Compressed_size);
 else
 Compressed_size = [585 855];
 I1_resized = imresize(imageL, Compressed_size); 
 I2_resized = imresize(imageR, Compressed_size); 
 %% calculate disparity maps
 [dispL,dispR] = get_disparityMap(I1_resized,I2_resized,[-150 150],...
                                   5,350,Compressed_size);
 end

end

 

